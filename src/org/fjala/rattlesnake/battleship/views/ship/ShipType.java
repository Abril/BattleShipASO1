/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wanda
 */
package org.fjala.rattlesnake.battleship.views.ship;

import org.fjala.rattlesnake.battleship.model.Orientation;

/**
 *
 * @author Maria Garcia
 */
public enum ShipType {

    CRUISE(3, "img/cruiseShipH.png", "img/cruiseShipV.png"),
    DESTROYER(2, "img/destroyerShipH.png", "img/destroyerShipV.png"),
    FRIGATE(1, "img/frigateShipH.png", "img/frigateShipV.png");

    private final int size;
    private final String horizontalModeImage;
    private final String verticalModeImage;
    private Orientation orientation;

    private ShipType(int size, String horizontalModeImage, String verticalModeImage) {
        this.size = size;
        this.horizontalModeImage = horizontalModeImage;
        this.verticalModeImage = verticalModeImage;
        orientation = Orientation.HORIZONTAL;
    }

    public int getSize() {
        return size;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    public String getImageByType() {
        return (orientation == Orientation.HORIZONTAL) ? horizontalModeImage : verticalModeImage;
    }
}

