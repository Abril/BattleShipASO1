/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.rattlesnake.battleship.model;

import java.util.EventObject;

/**
 *
 * @author Martin Zavaleta
 */
public class HitEvent extends EventObject {
    
    private final HitState state;
    
    public HitEvent(Box source) {
        super(source);
        state = source.getHitState();
    }
    
    public HitState getState() {
        return state;
    }
}
