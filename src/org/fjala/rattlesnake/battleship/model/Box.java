/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.rattlesnake.battleship.model;

import java.util.Objects;

/**
 *
 * @author Martin Zavaleta
 */
public class Box {

    public static enum State {

        EMPTY,
        FILLED
    }

    private final Coordinate position;
    private State state;
    private HitState hitState;
    private HitEventListener listener;

    public Box(Coordinate position) {
        this.position = position;
        state = State.EMPTY;
        hitState = HitState.UN_HIT;
    }

    public void setListener(HitEventListener listener) {
        this.listener = listener;
    }

    public Coordinate getPosition() {
        return position;
    }

    public boolean isEmpty() {
        return state == State.EMPTY;
    }

    public State getState() {
        return state;
    }

    public void fillBox() {
        state = State.FILLED;
    }

    public HitState getHitState() {
        return hitState;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.position);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Box other = (Box) obj;
        return Objects.equals(this.position, other.position);
    }

    public void acceptHit() throws HitException {
        if (hitState != HitState.UN_HIT) {
            throw new HitException();
        }

        hitState = HitState.HIT;
        HitEvent hit = new HitEvent(this);
        if (listener != null) {
            listener.onHitReceived(hit);
        }
    }

}
