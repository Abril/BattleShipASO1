/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fjala.rattlesnake.battleship.model;

import java.util.EventObject;

/**
 *
 * @author Maria Garcia
 */
public class ShipStateChangeEvent extends EventObject {

    private final Ship ship;

    public ShipStateChangeEvent(Ship source) {
        super(source);
        ship = source;
    }

    public Ship getShip() {
        return ship;
    }

}

