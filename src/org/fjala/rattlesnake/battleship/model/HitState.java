/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.rattlesnake.battleship.model;

/**
 *
 * @author Martin Zavaleta
 */
public enum HitState {
    
    UN_HIT,
    HIT
}
