/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.rattlesnake.battleship.model;

/**
 *
 * @author Martin Zavaleta
 */
public class HitException extends Exception {
    
    public HitException() {
        super("A box can't be hit more than once");
    }
}
