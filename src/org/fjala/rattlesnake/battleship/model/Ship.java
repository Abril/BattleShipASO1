/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fjala.rattlesnake.battleship.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.fjala.rattlesnake.battleship.views.ship.ShipType;

/**
 *
 * @author Jhonatan Arroyo
 * @author Maria Garcia
 */
public class Ship implements HitEventListener {

    public static enum State {

        ON_FLOAT,
        SUNKEN
    }

    private final Coordinate position;
    private final Orientation orientation;
    private final int size;
    private State state;
    private int availableHit;
    private ShipType type;
    private List<Box> boxes;
    private List<ShipStateChangeListener> listeners;

    public Ship(Coordinate coordinate, int size, Orientation orientation) {
        this.position = coordinate;
        this.orientation = orientation;
        boxes = new ArrayList<>(size);
        listeners = new ArrayList<>();
        this.size = size;
        availableHit = size;
        state = State.ON_FLOAT;
    }

    public Ship(Coordinate position, ShipType type) {
        this(position, type.getSize(), type.getOrientation());
        this.type = type;
    }

    public void addListener(ShipStateChangeListener listener) {
        listeners.add(listener);
    }

    public void addBox(Box box) {
        boxes.add(box);
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public int getSize() {
        return size;
    }

    public Coordinate getPosition() {
        return position;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.position);
        hash = 11 * hash + Objects.hashCode(this.type);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ship other = (Ship) obj;
        if (!Objects.equals(this.position, other.position)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    public ShipType getType() {
        return type;
    }

    public State getState() {
        return state;
    }

    public int getAvailableHit() {
        return availableHit;
    }

    @Override
    public void onHitReceived(HitEvent hit) {
        availableHit--;
        if (availableHit == 0) {
            state = State.SUNKEN;
            ShipStateChangeEvent sunk = new ShipStateChangeEvent(this);
            for (ShipStateChangeListener listener : listeners) {
                listener.onShipSunk(sunk);
            }
        }
    }
}
