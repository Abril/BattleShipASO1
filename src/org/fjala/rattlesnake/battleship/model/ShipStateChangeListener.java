/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wanda
 */
package org.fjala.rattlesnake.battleship.model;

/**
 *
 * @author Maria Garcia
 */
public interface ShipStateChangeListener {

    void onShipSunk(ShipStateChangeEvent event);
}
